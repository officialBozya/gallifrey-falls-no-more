﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Collections.ObjectModel;
using GallifreyFallsNoMore.Model;
using Xamarin.Forms;
using System.Windows.Input;
using GallifreyFallsNoMore.Model.Serialize;

namespace GallifreyFallsNoMore.ViewModel
{
    class LeaderboardViewModel : BaseViewModel
    {
        public ObservableCollection<LeaderboardField> LeaderboardFields { get; set; }
        public ICommand GoBackCommand { get; protected set; }
        public ICommand AppearingCommand { get; protected set; }
        public LeaderboardViewModel(INavigation navigation) : base(navigation)
        {
            LeaderboardFields = new ObservableCollection<LeaderboardField>();
            GoBackCommand = new Command(GoBack);
            AppearingCommand = new Command(OnAppearing);
        }
        private void GoBack()
        {
            Navigation.PopModalAsync();
        }
        private void OnAppearing()
        {
            LeaderboardFields.Clear();
            var serializer = new SerializerProxy();
            foreach (var field in serializer.DeserializeLeaderboard())
            {
                LeaderboardFields.Add(field);
            }
        }
    }
}
