﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GallifreyFallsNoMore.ViewModel
{
    class Caretaker
    {
        Stack<MementoPlayingField> history;
        MementoPlayingField last;
        public Caretaker()
        {
            history = new Stack<MementoPlayingField>();
        }
        public void Push(MementoPlayingField mementoPlayingField)
        {
            history.Push(mementoPlayingField);
            last = mementoPlayingField;
        }
        public MementoPlayingField Pop()
        {
            if (history.Count > 0)
                return history.Pop();
            else
                return null;
        }
        public MementoPlayingField GetLast()
        {
            return last;
        }
    }
}
