﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GallifreyFallsNoMore.ViewModel.DeathCoR
{
    interface IDeathHandler
    {
        IDeathHandler SetNext(IDeathHandler handler);
        bool DeathHandle(object request);
    }
}
