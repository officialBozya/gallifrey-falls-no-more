﻿using GallifreyFallsNoMore.Model.Buffs;
using System;
using System.Collections.Generic;
using System.Text;

namespace GallifreyFallsNoMore.ViewModel.DeathCoR
{
    class MoneyAntiDeathHandler : DefaultDeathHandler
    {
        public override bool DeathHandle(object request)
        {
            var field = request as PlayingFieldViewModel;
            if (field.Check(field.Money) != 0)
            {
                field.Money = 0.5;
                for (int i = 0; i < field.Player.Buffs.Count; i++)
                {
                    Buff buff = field.Player.Buffs[i];
                    if (buff is CoinBuff)
                        field.Player.Buffs.Remove(buff);
                }
                field.DeathHandler = new DefaultDeathHandler();
                foreach(var buff in field.Player.Buffs)
                {
                    buff.SetBuff(field);
                }
            }
            if (!field.DeathCalcualte())
            {
                return false;
            }
            else
            {
                return base.DeathHandle(request);
            }
        }
    }
}
