﻿using System;
using System.Collections.Generic;
using System.Text;
using GallifreyFallsNoMore.Model.Buffs;

namespace GallifreyFallsNoMore.ViewModel.DeathCoR
{
    class MilitaryAntiDeathHandler : DefaultDeathHandler
    {
        public override bool DeathHandle(object request)
        {
            var field = request as PlayingFieldViewModel;
            if (field.Check(field.MilitaryPower) != 0)
            {
                field.MilitaryPower = 0.5;
                for (int i = 0; i < field.Player.Buffs.Count; i++)
                {
                    Buff buff = field.Player.Buffs[i];
                    if (buff is WarTardisBuff)
                        field.Player.Buffs.Remove(buff);
                }
                field.DeathHandler = new DefaultDeathHandler();
                foreach (var buff in field.Player.Buffs)
                {
                    buff.SetBuff(field);
                }
            }
            if (!field.DeathCalcualte())
            {
                return false;
            }
            else
            {
                return base.DeathHandle(request);
            }
        }
    }
}
