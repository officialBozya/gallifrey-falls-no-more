﻿using GallifreyFallsNoMore.Model.Buffs;
using System;
using System.Collections.Generic;
using System.Text;

namespace GallifreyFallsNoMore.ViewModel.DeathCoR
{
    class ScienceAntiDeathHandler : DefaultDeathHandler
    {
        public override bool DeathHandle(object request)
        {
            var field = request as PlayingFieldViewModel;
            if (field.Check(field.Time) != 0)
            {
                field.Science = 0.5;
                for (int i = 0; i < field.Player.Buffs.Count; i++)
                {
                    Buff buff = field.Player.Buffs[i];
                    if (buff is SonicScrewdriverBuff)
                        field.Player.Buffs.Remove(buff);
                }
                field.DeathHandler = new DefaultDeathHandler();
                foreach (var buff in field.Player.Buffs)
                {
                    buff.SetBuff(field);
                }
            }
            if (!field.DeathCalcualte())
            {
                return false;
            }
            else
            {
                return base.DeathHandle(request);
            }
        }
    }
}
