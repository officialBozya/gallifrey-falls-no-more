﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GallifreyFallsNoMore.ViewModel
{
    abstract class PlayingMode
    {
        protected PlayingFieldViewModel _playingFieldViewModel;

        public void SetPlaingField(PlayingFieldViewModel playingFieldViewModel)
        {
            this._playingFieldViewModel = playingFieldViewModel;
        }
        public abstract void YearChange();
        public abstract void CardChange();
        public abstract void YearsChanged();
    }
}
