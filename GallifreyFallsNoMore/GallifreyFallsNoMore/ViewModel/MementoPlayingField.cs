﻿using System;
using System.Collections.Generic;
using System.Text;
using GallifreyFallsNoMore.Model;
using System.Collections.ObjectModel;
using GallifreyFallsNoMore.ViewModel.DeathCoR;

namespace GallifreyFallsNoMore.ViewModel
{
    class MementoPlayingField
    {
        private Player _player;
        private ObservableCollection<Card> _cards;
        private IDeathHandler _deathHandler;
        private IStrategy _strategy;
        private PlayingMode _playingMode;
        public MementoPlayingField(Player player, ObservableCollection<Card> cards, IDeathHandler deathHandler,
            IStrategy strategy, PlayingMode playingMode)
        {
            _player = player;
            _cards = cards;
            _deathHandler = deathHandler;
            _strategy = strategy;
            _playingMode = playingMode;
        }
        public Player GetPlayer()
        {
            return _player;
        }
        public ObservableCollection<Card> GetCards()
        {
            return _cards;
        }
        public IDeathHandler GetDeathHandler()
        {
            return _deathHandler;
        }
        public IStrategy GetStrategy()
        {
            return _strategy;
        }
        public PlayingMode GetPlayingMode()
        {
            return _playingMode;
        }
    }
}
