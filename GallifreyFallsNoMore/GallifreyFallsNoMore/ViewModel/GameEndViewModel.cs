﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using GallifreyFallsNoMore.Model;
using GallifreyFallsNoMore.Model.Serialize;
using System.Collections.ObjectModel;
using Xamarin.Forms;
using System.Windows.Input;
using GallifreyFallsNoMore.View;

namespace GallifreyFallsNoMore.ViewModel
{
    class GameEndViewModel : BaseViewModel
    {
        public ObservableCollection<LeaderboardField> LeaderboardFields { get; set; }
        public ICommand GoStartCommand { get; protected set; }
        public ICommand TryAgainCommand { get; protected set; }
        public ICommand AppearingCommand { get; protected set; }
        public GameEndViewModel(INavigation navigation):base(navigation)
        {
            LeaderboardFields = new ObservableCollection<LeaderboardField>();
            GoStartCommand = new Command(GoStart);
            TryAgainCommand = new Command(TryAgain);
            AppearingCommand = new Command(OnAppearing);
        }
        private void GoStart()
        {
            Navigation.PopModalAsync();
        }
        private void TryAgain()
        {
            StartPage.GetPage().TryAgain = true;
            GoStart();
        }
        private void OnAppearing()
        {
            LeaderboardFields.Clear();
            var serializer = new SerializerProxy();
            foreach(var field in serializer.DeserializeLeaderboard())
            {
                LeaderboardFields.Add(field);
            }
        }
    }
}
