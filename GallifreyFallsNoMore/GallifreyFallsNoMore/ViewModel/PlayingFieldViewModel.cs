﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Collections.ObjectModel;
using GallifreyFallsNoMore.Model;
using Xamarin.Forms;
using System.Windows.Input;
using MLToolkit.Forms.SwipeCardView.Core;
using GallifreyFallsNoMore.View;
using GallifreyFallsNoMore.Model.Serialize;
using GallifreyFallsNoMore.ViewModel.DeathCoR;
using GallifreyFallsNoMore.Model.Buffs;
using GallifreyFallsNoMore.Model.Collections;
using MLToolkit.Forms.SwipeCardView;

namespace GallifreyFallsNoMore.ViewModel
{
    internal class PlayingFieldViewModel : BaseViewModel
    {
        private CardsCollection _basicCardsCollection;
        private CardsCollection _questTriggersColection;
        private BasicIterator _basicIterator;
        private QuestTriggerIterator _questTriggerIterator;
        private Answer _answer;
        private uint _cardCount;
        private ObservableCollection<Card> _cards = new ObservableCollection<Card>();
        public ObservableCollection<Card> Cards
        {
            get { return _cards; }
            set
            {
                _cards = value;
                RaisePropertyChanged();
            }
        }
        private IDeathHandler _deathHandler;
        private IStrategy _strategy;
        private Caretaker _caretaker;
        private PlayingMode _playingMode;
        private uint _threshold;
        public bool GameEnded { get; set; }
        public Player Player 
        {
            get => _player; 
            private set 
            {
                if (_player != value)
                {
                    _player = value;
                    RaisePropertyChanged();
                }
            } 
        }
        public SwipeCardView SwipeCardView { get; set; }
        public IDeathHandler DeathHandler
        {
            get => _deathHandler;
            set => _deathHandler = value;        
        }
        public ICommand OnSwipedCommand { get; }
        public ICommand OnDraggingCommand { get; }
        private Card Card { get; set; }
        private string[] influence;
        public double Science
        {
            get { return Player.Stats.Science; }
            set
            {
                if (Player.Stats.Science != value)
                {
                    Player.Stats.Science = value;
                    RaisePropertyChanged();
                }
            }
        }
        public double Time
        {
            get { return Player.Stats.Time; }
            set
            {
                if (Player.Stats.Time != value)
                {
                    Player.Stats.Time = value;
                    RaisePropertyChanged();
                }
            }
        }
        public double MilitaryPower
        {
            get { return Player.Stats.MilitaryPower; }
            set
            {
                if (Player.Stats.MilitaryPower != value)
                {
                    Player.Stats.MilitaryPower = value;
                    RaisePropertyChanged();
                }
            }
        }
        public double Money
        {
            get { return Player.Stats.Money; }
            set
            {
                if (Player.Stats.Money != value)
                {
                    Player.Stats.Money = value;
                    RaisePropertyChanged();
                }
            }
        }
        public short Years
        {
            get { return Player.Years; }
            set
            {
                Player.Years = value;
                YearsStr = value.ToString();
                _playingMode.YearsChanged();
                RaisePropertyChanged();
            }
        }
        string _yearsStr;
        public string YearsStr
        {
            get { return _yearsStr; }
            set
            {
                _yearsStr = "Year " + value;
                RaisePropertyChanged();
            }
        }
        public uint Threshold
        {
            get => _threshold;
            set
            {
                _threshold = value;
                RaisePropertyChanged();
            }
        }
        private Card _topCard;
        private Player _player;

        public Card TopCard
        {
            get => _topCard;
            set
            {
                _topCard = value;
                RaisePropertyChanged();
            }
        }
        public string InfluenceScience
        {
            get => influence[0];
            set
            {
                influence[0] = value;
                RaisePropertyChanged();
            }
        }
        public string InfluenceTime
        {
            get => influence[1];
            set
            {
                influence[1] = value;
                RaisePropertyChanged();
            }
        }
        public string InfluenceMilitaryPower
        {
            get => influence[2];
            set
            {
                influence[2] = value;
                RaisePropertyChanged();
            }
        }
        public string InfluenceMoney
        {
            get => influence[3];
            set
            {
                influence[3] = value;
                RaisePropertyChanged();
            }
        }
        public Caretaker Caretaker
        {
            get => _caretaker;
            set => _caretaker = value;
        }
        public ObservableCollection<Buff> Buffs
        {
            get => Player.Buffs;
            set
            {
                Player.Buffs = value;
                RaisePropertyChanged();
            }
        }

        internal BasicIterator BasicIterator 
        { 
            get => _basicIterator; 
            set => _basicIterator = value; 
        }
        public Answer Answer 
        { 
            get => _answer; 
            set => _answer = value; 
        }
        internal QuestTriggerIterator QuestTriggerIterator 
        { 
            get => _questTriggerIterator; 
            set => _questTriggerIterator = value; 
        }

        public PlayingFieldViewModel(INavigation navigation) : base(navigation)
        {
            Player = new Player();
            Buffs = new ObservableCollection<Buff>();
            Caretaker = new Caretaker();
            _strategy = new DefaultStrategy();
            DeathHandler = new DefaultDeathHandler();
            _basicCardsCollection = new CardsCollection("CummonPack", PackType.Basic);
            _questTriggersColection = new CardsCollection("QuestTriggers", PackType.QuestTrigger);
            BasicIterator = (BasicIterator)_basicCardsCollection.GetEnumerator();
            QuestTriggerIterator = (QuestTriggerIterator)_questTriggersColection.GetEnumerator();
            GameEnded = false;
            influence = new string[4];
            Threshold = (uint)(App.ScreenWidth / 3);
            Cards = new ObservableCollection<Card>();
            OnSwipedCommand = new Command<SwipedCardEventArgs>(SwipedCommand);
            OnDraggingCommand = new Command<DraggingCardEventArgs>(DraggingCommand);
            this.TransitionTo(new QuestMode("Start", this));
            Years = 0;
            _cardCount = 0;
        }
        private void SwipedCommand(SwipedCardEventArgs eventArgs)
        {
            _cardCount++;
            SwipeCardDirection direction;
            switch (eventArgs.Direction)
            {
                case SwipeCardDirection.Right:
                    {
                        Money += ((Card)eventArgs.Item).InfluenceYes.Money;
                        Science += ((Card)eventArgs.Item).InfluenceYes.Science;
                        Time += ((Card)eventArgs.Item).InfluenceYes.Time;
                        MilitaryPower += ((Card)eventArgs.Item).InfluenceYes.MilitaryPower;
                        Answer = Answer.Yes;
                        direction = SwipeCardDirection.Left;
                        if(eventArgs.Item is CardQuestTrigger)
                        {
                            TransitionTo(((CardQuestTrigger)eventArgs.Item).Quest(this));
                        }
                        break;
                    }
                case SwipeCardDirection.Left:
                    {
                        Money += ((Card)eventArgs.Item).InfluenceNo.Money;
                        Science += ((Card)eventArgs.Item).InfluenceNo.Science;
                        Time += ((Card)eventArgs.Item).InfluenceNo.Time;
                        MilitaryPower += ((Card)eventArgs.Item).InfluenceNo.MilitaryPower;
                        Answer = Answer.No;
                        direction = SwipeCardDirection.Right;
                        break;
                    }
                default:
                    throw new ArgumentOutOfRangeException();
            }
            if (DeathCalcualte() && DeathHandler.DeathHandle(this)) 
            {
                    Navigation.PushModalAsync(GameEndPage.GetPage());
            }
            else
            {
                _playingMode.YearChange();
                if (_cardCount % 2 == 1)
                {
                    _playingMode.CardChange();
                    SwipeCardView.InvokeSwipe(direction, 20, 10, TimeSpan.FromMilliseconds(1), TimeSpan.FromMilliseconds(2));
                    if (_playingMode is NormalMode)
                    {
                        Years--;
                    }
                }
            }
        }
        private void DraggingCommand(DraggingCardEventArgs e)
        {
            Card card = (Card)e.Item;
            switch (e.Direction)
            {
                case SwipeCardDirection.Left:
                    InfluenceScience = Influence(card.InfluenceNo.Science);
                    InfluenceTime = Influence(card.InfluenceNo.Time);
                    InfluenceMilitaryPower = Influence(card.InfluenceNo.MilitaryPower);
                    InfluenceMoney = Influence(card.InfluenceNo.Money);
                    break;
                case SwipeCardDirection.Right:
                    InfluenceScience = Influence(card.InfluenceYes.Science);
                    InfluenceTime = Influence(card.InfluenceYes.Time);
                    InfluenceMilitaryPower = Influence(card.InfluenceYes.MilitaryPower);
                    InfluenceMoney = Influence(card.InfluenceYes.Money);
                    break;
                case SwipeCardDirection.Down:
                    break;
                case SwipeCardDirection.None:
                    break;
                case SwipeCardDirection.Up:
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
        public int Check(double stat)
        {
            if (stat < 0.01)
            {
                return -1;
            }
            else if (stat > 0.99)
            {
                return 1;
            }
            return 0;
        }
        private string Influence(double stat)
        {
            return this._strategy.InfluenceAlgorithm(stat);
        }
        public void SetStrategy(IStrategy strategy)
        {
            this._strategy = strategy;
        }
        public MementoPlayingField Save()
        {
            return new MementoPlayingField(Player, Cards, DeathHandler, _strategy, _playingMode);
        }
        public void Restore(MementoPlayingField m)
        {
            Player = m.GetPlayer();
            Cards = m.GetCards();
            DeathHandler = m.GetDeathHandler();
            SetStrategy(m.GetStrategy());
            TransitionTo(m.GetPlayingMode());
        }
        public void TransitionTo(PlayingMode playingMode)
        {
            this._playingMode = playingMode;
            this._playingMode.SetPlaingField(this);
        }
        public string DeathCousedBy()
        {
            if (Check(Science) == 1)
            {
                return "The Moment rebelled and destroyed everyone.";
            }
            else if (Check(Science) == -1)
            {
                return "The enemies used the latest weapons against us. Our scientists have nothing to answer... (At this moment you are blinded by the blue light and you die)";
            }
            else if (Check(Time) == 1)
            {
                return "Time lords realized that they were out of time and killed you.";
            }
            else if (Check(Time) == -1)
            {
                return "Completely losing track of time you go crazy and jump out the window.";
            }
            else if (Check(MilitaryPower) == 1)
            {
                return "The general makes a military coup, you are imprisoned where you commit suicide so as not to suffer torture.";
            }
            else if (Check(MilitaryPower) == -1)
            {
                return "Daleks attacked us. Our army cannot continue the battle. It looks like the end ... (They extreminate your power shields and your head)";
            }
            else if (Check(Money) == 1)
            {
                return "The treasury is overcrowded, so we can start living on a new planet and blow this up with our enemies. (That's what your bankers thought, but they decided to leave you to die)";
            }
            else if (Check(Money) == -1)
            {
                return "We have run out of money. People are hungry and up against you. (At this moment a detachment of Communist rebels breaks in and kills you)";
            }
            else return string.Empty;
        }
        public bool DeathCalcualte()
        {
            return ((Math.Abs(Check(Money)) + Math.Abs(Check(Science)) + Math.Abs(Check(Time)) + Math.Abs(Check(MilitaryPower))) != 0);
        }
    }
}
