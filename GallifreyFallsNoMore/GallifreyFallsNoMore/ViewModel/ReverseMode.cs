﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GallifreyFallsNoMore.ViewModel
{
    class ReverseMode : PlayingMode
    {
        bool negative = false;
        public override void CardChange()
        {
            if (negative)
                _playingFieldViewModel.TransitionTo(new QuestMode("null", _playingFieldViewModel));
        }

        public override void YearChange()
        {
            var memento = _playingFieldViewModel.Caretaker.Pop();
            if (memento != null)
                _playingFieldViewModel.Restore(memento);
            else
            {
                _playingFieldViewModel.Years--;
                negative = true;
            }        
        }

        public override void YearsChanged()
        {

        }
    }
}
