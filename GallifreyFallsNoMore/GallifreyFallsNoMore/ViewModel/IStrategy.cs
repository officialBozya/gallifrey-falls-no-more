﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GallifreyFallsNoMore.ViewModel
{
    interface IStrategy
    {
        string InfluenceAlgorithm(double stat);
    }
}
