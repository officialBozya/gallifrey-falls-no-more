﻿using GallifreyFallsNoMore.Model;
using GallifreyFallsNoMore.Model.Buffs;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace GallifreyFallsNoMore.ViewModel
{
    class NormalMode : PlayingMode
    {
        public override void CardChange()
        {
            var rand = new Random();
            if (_playingFieldViewModel.Years % 2 == 0 && /*rand.Next(10) == 3 &&*/
                    _playingFieldViewModel.QuestTriggerIterator.MoveNext() && BuffNoExiest(((CardQuestTrigger)_playingFieldViewModel.QuestTriggerIterator.Current()).QuestName)) 
            {
                    _playingFieldViewModel.Cards.Add((Card)_playingFieldViewModel.QuestTriggerIterator.Current());
                    _playingFieldViewModel.Cards.Add(Card.GetJacket());
            }
            else
            {
                _playingFieldViewModel.BasicIterator.MoveNext();
                _playingFieldViewModel.Cards.Add((Card)_playingFieldViewModel.BasicIterator.Current());
                _playingFieldViewModel.Cards.Add(Card.GetJacket());
            }
        }

        public override void YearChange()
        {
            _playingFieldViewModel.Years++;
        }

        public override void YearsChanged()
        {
            if (_playingFieldViewModel.Years % 10 == 0)
            {
                _playingFieldViewModel.Caretaker.Push(_playingFieldViewModel.Save());
            }
        }
        private bool BuffNoExiest(string buffName)
        {
            foreach(var buff in _playingFieldViewModel.Buffs)
            {
                if (buff.Name == buffName) return false;
            }
            return true;
        }
    }
}
