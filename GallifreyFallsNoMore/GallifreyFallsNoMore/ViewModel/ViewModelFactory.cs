﻿using MLToolkit.Forms.SwipeCardView;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace GallifreyFallsNoMore.ViewModel
{
    class ViewModelFactory : IAbstractFactoryViewModel
    {
        public BaseViewModel CreateGameEnd(INavigation navigation)
        {
            return new GameEndViewModel(navigation);
        }

        public BaseViewModel CreateLeaderboard(INavigation navigation)
        {
            return new LeaderboardViewModel(navigation);
        }

        public BaseViewModel CreatePlayingField(INavigation navigation, SwipeCardView swipeCardView)
        {
            return new PlayingFieldViewModel(navigation) { SwipeCardView = swipeCardView };
        }

        public BaseViewModel CreateStart(INavigation navigation)
        {
            return new StartViewModel(navigation);
        }
    }
}
