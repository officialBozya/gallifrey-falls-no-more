﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GallifreyFallsNoMore.Model
{
    public class Stats : ICloneable 
    {
        public double Science { get; set; }
        public double Time { get; set; }
        public double MilitaryPower { get; set; }
        public double Money { get; set; }
        public ICloneable Clone()
        {
            Stats clone = new Stats() { 
                MilitaryPower = this.MilitaryPower, 
                Money = this.Money, 
                Science = this.Science, 
                Time = this.Time 
            };
            return clone;
        }
    }
}
