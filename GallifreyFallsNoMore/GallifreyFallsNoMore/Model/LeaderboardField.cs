﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GallifreyFallsNoMore.Model
{
    public class LeaderboardField
    {
        public int ID { get; set; }
        public string DeathReason { get; set; }
        public int YearsLived { get; set; }
    }
}
