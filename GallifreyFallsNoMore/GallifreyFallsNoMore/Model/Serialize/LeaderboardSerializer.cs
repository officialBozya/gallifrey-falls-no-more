﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GallifreyFallsNoMore.Model.Serialize
{
    class LeaderboardSerializer:SerializersDecorator<LeaderboardField>
    {
        public LeaderboardSerializer(AbstractSerializer<LeaderboardField> serializer) : base(serializer) { }
        public override List<LeaderboardField> Deserialize(string packName = "leaderboard")
        {
            return base.Deserialize(packName);
        }

        public override void Serialize(List<LeaderboardField> items, string packName = "leaderboard")
        {
            base.Serialize(items, packName);
        }
    }
}
