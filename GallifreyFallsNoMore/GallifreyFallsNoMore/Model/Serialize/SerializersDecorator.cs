﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GallifreyFallsNoMore.Model.Serialize
{
    class SerializersDecorator<T> : AbstractSerializer<T>
    {
        protected AbstractSerializer<T> _abstractSerializer;
        public SerializersDecorator(AbstractSerializer<T> serializer)
        {
            _abstractSerializer = serializer;
        }
        public AbstractSerializer<T> Serializer
        {
            protected get { return _abstractSerializer; }
            set => _abstractSerializer = value;
        }
        public override void Serialize(List<T> items, string packName)
        {
            if (this._abstractSerializer != null)
            {
                _abstractSerializer.Serialize(items, packName);
            }
        }

        public override List<T> Deserialize(string packName)
        {
            if (this._abstractSerializer != null)
                return _abstractSerializer.Deserialize(packName);
            else
                return null;
        }
        
    }
}
