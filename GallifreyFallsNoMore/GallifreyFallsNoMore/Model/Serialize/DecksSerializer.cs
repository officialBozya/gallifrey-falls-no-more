﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GallifreyFallsNoMore.Model.Serialize
{
    class DecksSerializer:SerializersDecorator<string>
    {
        public DecksSerializer(AbstractSerializer<string> serializer) : base(serializer) { }
        public override List<string> Deserialize(string packName = "decks")
        {
            return base.Deserialize(packName);
        }

        public override void Serialize(List<string> items, string packName = "decks")
        {
            base.Serialize(items, packName);
        }
    }
}
