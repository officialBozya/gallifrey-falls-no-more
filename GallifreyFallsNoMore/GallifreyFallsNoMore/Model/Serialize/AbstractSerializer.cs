﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GallifreyFallsNoMore.Model.Serialize
{
    public abstract class AbstractSerializer<T>
    {
        public abstract void Serialize(List<T> items, string packName);
        public abstract List<T> Deserialize(string packName);
    }
}
