﻿using GallifreyFallsNoMore.ViewModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace GallifreyFallsNoMore.Model
{
    internal class CardQuestTrigger:Card
    {
        public string QuestName { get; set; }
        public QuestMode Quest(PlayingFieldViewModel playingFieldViewModel)
        {
            return new QuestMode(QuestName, playingFieldViewModel);
        }
    }
}
