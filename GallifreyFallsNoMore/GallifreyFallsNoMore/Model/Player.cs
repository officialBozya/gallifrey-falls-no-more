﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using GallifreyFallsNoMore.Model.Buffs;

namespace GallifreyFallsNoMore.Model
{
    class Player
    {
        public Player()
        {
            Stats = new Stats() { Time = 0.5, Science = 0.5, MilitaryPower = 0.5, Money = 0.5 };
            Years = 0;
        }
        public Stats Stats { get; set; }
        public ObservableCollection<Buff> Buffs { get; set; }
        public short Years { get; set; }
    }
}
