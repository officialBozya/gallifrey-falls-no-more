﻿using System;
using System.Collections.Generic;
using System.Text;
using GallifreyFallsNoMore.ViewModel;
using GallifreyFallsNoMore.ViewModel.DeathCoR;

namespace GallifreyFallsNoMore.Model.Buffs
{
    abstract class AntiDeathBuff:Buff
    {
        protected IDeathHandler _deathHandler;
        public override void SetBuff(object sender)
        {
            var field = sender as PlayingFieldViewModel;
            _deathHandler.SetNext(field.DeathHandler);
            field.DeathHandler = _deathHandler;
        }
    }
}
