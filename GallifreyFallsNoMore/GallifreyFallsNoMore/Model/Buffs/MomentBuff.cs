﻿using System;
using System.Collections.Generic;
using System.Text;
using GallifreyFallsNoMore.ViewModel.DeathCoR;

namespace GallifreyFallsNoMore.Model.Buffs
{
    class MomentBuff : AntiDeathBuff
    {
        public MomentBuff()
        {
            ImagePath = "Assets/Buffs/Moment.png";
            _deathHandler = new TimeAntiDeathHandler();
            Name = "";
        }
    }
}
