﻿using System;
using System.Collections.Generic;
using System.Text;
using GallifreyFallsNoMore.ViewModel.DeathCoR;

namespace GallifreyFallsNoMore.Model.Buffs
{
    class WarTardisBuff : AntiDeathBuff
    {
        public WarTardisBuff()
        {
            ImagePath = "Assets/Buffs/WarTardis.png";
            _deathHandler = new MilitaryAntiDeathHandler();
            Name = "";
        }
    }
}
