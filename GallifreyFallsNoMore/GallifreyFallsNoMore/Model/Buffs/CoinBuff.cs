﻿using System;
using System.Collections.Generic;
using System.Text;
using GallifreyFallsNoMore.ViewModel.DeathCoR;

namespace GallifreyFallsNoMore.Model.Buffs
{
    class CoinBuff : AntiDeathBuff
    {
        public CoinBuff()
        {
            ImagePath = "Assets/Buffs/CoinTardis.png";
            _deathHandler = new MoneyAntiDeathHandler();
            Name = "";
        }
    }
}
