﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace GallifreyFallsNoMore.Model.Collections
{
    abstract class IteratorAggregate : IEnumerable
    {
        public abstract IEnumerator GetEnumerator();
    }
}
