﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GallifreyFallsNoMore.Model.Collections
{
    class QuestTriggerIterator : Iterator
    {
        private CardsCollection _collection;
        private int _position = -1;
        public QuestTriggerIterator(CardsCollection collection)
        {
            this._collection = collection;
        }
        public override object Current()
        {
            var item = this._collection.GetItems()[_position];
            return item;
        }

        public override int Key()
        {
            return this._position;
        }

        public override bool MoveNext()
        {
            if (_position >= 0 && _collection.GetItems().Count > 0)
            {
                this._collection.GetItems().RemoveAt(_position);
            }
            if (_collection.GetItems().Count == 1) 
            {
                _position = 0;
                return true;
            }
            else if (_collection.GetItems().Count > 1)
            {
                var randomizer = new Random();
                int updatedPosition = randomizer.Next(_collection.GetItems().Count);
                if (updatedPosition == _position)
                {
                    MoveNext();
                    return true;
                }
                else
                {
                    _position = updatedPosition;
                    return true;
                }
            }
            else
                return false;
        }

        public override void Reset()
        {
            _position = 0;
        }
    }
}
