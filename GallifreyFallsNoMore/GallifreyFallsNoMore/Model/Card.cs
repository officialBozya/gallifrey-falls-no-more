﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using System.IO;

namespace GallifreyFallsNoMore.Model
{
    public class Card : ICloneable 
    {
        public string ImagePath { get; set; }
        public string Text { get; set; }
        public string Yes { get; set; }
        public string No { get; set; }
        public Stats InfluenceYes { get; set; }
        public Stats InfluenceNo { get; set; }
        public ICloneable Clone()
        {
            Card clone = new Card()
            {
                ImagePath = this.ImagePath,
                Text = this.Text,
                Yes = this.Yes,
                No = this.No,
                InfluenceNo = (Stats)this.InfluenceNo.Clone(),
                InfluenceYes = (Stats)this.InfluenceYes.Clone()
            };
            return clone;
        }
        static public Card GetJacket()
        {
            return new Card() { ImagePath = "Assets/Cards/Jacket.jpg", InfluenceNo = new Stats() { MilitaryPower = 0, Money = 0, Science = 0, Time = 0 }, InfluenceYes = new Stats() { MilitaryPower = 0, Money = 0, Science = 0, Time = 0 }, No = "", Text = "", Yes = "" };
        }
    }
}
