[{
	"ImagePath":"Assets/Cards/Ohila.png",
	"Text":"Time vortex is unstable. Let me correct this.",
	"Yes":"Yes, sure",
	"No":"I don't care",
	"InfluenceYes":
	{
		"Science":0,
		"Time":0.1,
		"MilitaryPower":0,
		"Money":-0.1
	},
	"InfluenceNo":
	{
		"Science":0,
		"Time":-0.2,
		"MilitaryPower":0,
		"Money":0
	}
},
{
	"ImagePath":"Assets/Cards/Pandar.png",
	"Text":"I get gossip that some of our soldiers are stealing the treasure trove. You have to do something.",
	"Yes":"I'll talk to the general.",
	"No":"No, it can't be.",
	"InfluenceYes":
	{
		"Science":0,
		"Time":0,
		"MilitaryPower":-0.2,
		"Money":0.1
	},
	"InfluenceNo":
	{
		"Science":-0.1,
		"Time":-0.1,
		"MilitaryPower":0.1,
		"Money":-0.2
	}
},
{
	"ImagePath":"Assets/Cards/General.png",
	"Text":"The daleks are attacking us. The Army needs your support.",
	"Yes":"Give'em hell, sir.",
	"No":"War has exhausted us.",
	"InfluenceYes":
	{
		"Science":0.1,
		"Time":0.1,
		"MilitaryPower":0.2,
		"Money":-0.1
	},
	"InfluenceNo":
	{
		"Science":-0.1,
		"Time":-0.1,
		"MilitaryPower":-0.2,
		"Money":0
	}
},
{
	"ImagePath":"Assets/Cards/Romana_2.png",
	"Text":"You need to increase funding for the science department, or soon our citizens will forget how to use the matrix.",
	"Yes":"Yes of course.",
	"No":"You exaggerate.",
	"InfluenceYes":
	{
		"Science":0.2,
		"Time":0,
		"MilitaryPower":0,
		"Money":-0.1
	},
	"InfluenceNo":
	{
		"Science":-0.2,
		"Time":0,
		"MilitaryPower":0,
		"Money":0
	}
},
{
	"ImagePath":"Assets/Cards/Ohila.png",
	"Text":"A cleverboy threw a dead dalek into a time vortex. And now its remains appear in different time intervals. What should we do?",
	"Yes":"The military will correct it.",
	"No":"The scientists will correct it.",
	"InfluenceYes":
	{
		"Science":0,
		"Time":0.1,
		"MilitaryPower":-0.1,
		"Money":0
	},
	"InfluenceNo":
	{
		"Science":-0.1,
		"Time":0.1,
		"MilitaryPower":0,
		"Money":0
	}
},
{
	"ImagePath":"Assets/Cards/Pandar.png",
	"Text":"Someone wants to buy our research. He is willing to pay generously.",
	"Yes":"I'll talk to Romana.",
	"No":"No, this is unacceptable.",
	"InfluenceYes":
	{
		"Science":-0.2,
		"Time":0,
		"MilitaryPower":0,
		"Money":0.2
	},
	"InfluenceNo":
	{
		"Science":0.1,
		"Time":0,
		"MilitaryPower":0,
		"Money":-0.1
	}
},
{
	"ImagePath":"Assets/Cards/General.png",
	"Text":"In my opinion, army needs more funding.",
	"Yes":"Yes of course.",
	"No":"We have no money.",
	"InfluenceYes":
	{
		"Science":0,
		"Time":0,
		"MilitaryPower":0.1,
		"Money":-0.1
	},
	"InfluenceNo":
	{
		"Science":0,
		"Time":0,
		"MilitaryPower":-0.2,
		"Money":0
	}
},
{
	"ImagePath":"Assets/Cards/Romana_2.png",
	"Text":"We have developed a new way of moving matter. Where to spend profit?",
	"Yes":"Keep exploring.",
	"No":"Transfer everything to the treasury.",
	"InfluenceYes":
	{
		"Science":0.2,
		"Time":0.1,
		"MilitaryPower":0.1,
		"Money":0
	},
	"InfluenceNo":
	{
		"Science":0,
		"Time":0,
		"MilitaryPower":0,
		"Money":0.2
	}
},
{
	"ImagePath":"Assets/Cards/Ohila.png",
	"Text":"The military pressures on us and demands that we allow them to use a time vortex against the daleks. Do something!",
	"Yes":"I'll talk to the General.",
	"No":"I agree with the General.",
	"InfluenceYes":
	{
		"Science":0,
		"Time":0.1,
		"MilitaryPower":-0.1,
		"Money":0
	},
	"InfluenceNo":
	{
		"Science":0,
		"Time":-0.2,
		"MilitaryPower":0.2,
		"Money":0
	}
},
{
	"ImagePath":"Assets/Cards/Pandar.png",
	"Text":"People are starving, allocate funds to support those in need.",
	"Yes":"Yes of course.",
	"No":"These are their problems.",
	"InfluenceYes":
	{
		"Science":0.1,
		"Time":0.1,
		"MilitaryPower":0.1,
		"Money":-0.2
	},
	"InfluenceNo":
	{
		"Science":-0.1,
		"Time":-0.1,
		"MilitaryPower":-0.1,
		"Money":0
	}
},
{
	"ImagePath":"Assets/Cards/General.png",
	"Text":"We captured several daleks. What to do with them?",
	"Yes":"Give it to scientists for research.",
	"No":"Sell ​​for parts.",
	"InfluenceYes":
	{
		"Science":0.1,
		"Time":0,
		"MilitaryPower":0.1,
		"Money":0
	},
	"InfluenceNo":
	{
		"Science":0,
		"Time":0,
		"MilitaryPower":0,
		"Money":0.2
	}
},
{
	"ImagePath":"Assets/Cards/Romana_2.png",
	"Text":"We run out of equipment. Please give us money.",
	"Yes":"Yes of course.",
	"No":"You exaggerate.",
	"InfluenceYes":
	{
		"Science":0.1,
		"Time":0,
		"MilitaryPower":0,
		"Money":-0.1
	},
	"InfluenceNo":
	{
		"Science":-0.2,
		"Time":0,
		"MilitaryPower":0,
		"Money":0
	}
}]