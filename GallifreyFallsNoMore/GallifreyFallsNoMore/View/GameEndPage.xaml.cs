﻿using GallifreyFallsNoMore.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace GallifreyFallsNoMore.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class GameEndPage : ContentPage
    {
        GameEndPage()
        {
            InitializeComponent();
            IAbstractFactoryViewModel factoryViewModel = new ViewModelFactory();
            BindingContext = factoryViewModel.CreateGameEnd(this.Navigation);
        }
        private static GameEndPage _instance = null;
        private static readonly object _lock = new object();
        public static GameEndPage GetPage()
        {
            if (_instance == null)
            {
                lock (_lock)
                {
                    if (_instance == null)
                    {
                        _instance = new GameEndPage();
                    }
                }
            }
            return _instance;
        }
        protected override void OnAppearing()
        {
            base.OnAppearing();
            ((GameEndViewModel)(BindingContext)).AppearingCommand.Execute(null);
        }
    }
}