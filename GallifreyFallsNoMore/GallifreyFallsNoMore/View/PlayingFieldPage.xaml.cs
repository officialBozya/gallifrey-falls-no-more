﻿using GallifreyFallsNoMore.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MLToolkit.Forms.SwipeCardView;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using MLToolkit.Forms.SwipeCardView.Core;

namespace GallifreyFallsNoMore.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PlayingFieldPage : ContentPage
    {
        public PlayingFieldPage()
        {
            InitializeComponent();
            IAbstractFactoryViewModel factoryViewModel = new ViewModelFactory();
            BindingContext = factoryViewModel.CreatePlayingField(this.Navigation, this.SwipeCardView); 
            SwipeCardView.Dragging += OnDragging;
        }
        private void OnDragging(object sender, DraggingCardEventArgs e)
        {
            var view = (Xamarin.Forms.View)sender;
            var nopeFrame = view.FindByName<Frame>("NopeFrame");
            var yesFrame = view.FindByName<Frame>("YesFrame");
            var science = this.FindByName<Label>("Science");
            var time = this.FindByName<Label>("Time");
            var militaryPower = this.FindByName<Label>("MilitaryPower");
            var money = this.FindByName<Label>("Money");
            var threshold = (BindingContext as PlayingFieldViewModel).Threshold;

            var draggedXPercent = e.DistanceDraggedX / threshold;

            switch (e.Position)
            {
                case DraggingCardPosition.Start:
                    nopeFrame.Opacity = 0;
                    yesFrame.Opacity = 0;
                    science.Opacity = 0;
                    time.Opacity = 0;
                    militaryPower.Opacity = 0;
                    money.Opacity = 0;
                    break;

                case DraggingCardPosition.UnderThreshold:
                    if (e.Direction == SwipeCardDirection.Left)
                    {
                        nopeFrame.Opacity = (-1) * draggedXPercent;
                        science.Opacity = (-1) * draggedXPercent;
                        time.Opacity = (-1) * draggedXPercent;
                        militaryPower.Opacity = (-1) * draggedXPercent;
                        money.Opacity = (-1) * draggedXPercent;
                    }
                    else if (e.Direction == SwipeCardDirection.Right)
                    {
                        yesFrame.Opacity = draggedXPercent;
                        science.Opacity = draggedXPercent;
                        time.Opacity = draggedXPercent;
                        militaryPower.Opacity = draggedXPercent;
                        money.Opacity = draggedXPercent;
                    }
                    break;

                case DraggingCardPosition.OverThreshold:
                    if (e.Direction == SwipeCardDirection.Left)
                    {
                        nopeFrame.Opacity = 1;
                        yesFrame.Opacity = 0;
                    }
                    else if (e.Direction == SwipeCardDirection.Right)
                    {
                        yesFrame.Opacity = 1;
                        nopeFrame.Opacity = 0;
                    }
                    science.Opacity = 1;
                    time.Opacity = 1;
                    militaryPower.Opacity = 1;
                    money.Opacity = 1;
                    break;

                case DraggingCardPosition.FinishedUnderThreshold:
                    nopeFrame.Opacity = 0;
                    yesFrame.Opacity = 0;
                    science.Opacity = 0;
                    time.Opacity = 0;
                    militaryPower.Opacity = 0;
                    money.Opacity = 0;
                    break;

                case DraggingCardPosition.FinishedOverThreshold:
                    nopeFrame.Opacity = 0;
                    yesFrame.Opacity = 0;
                    science.Opacity = 0;
                    time.Opacity = 0;
                    militaryPower.Opacity = 0;
                    money.Opacity = 0;
                    break;

                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
        protected override void OnAppearing()
        {
            base.OnAppearing();
            if (((PlayingFieldViewModel)BindingContext).GameEnded)
                Navigation.PopModalAsync();
        }
    }
}